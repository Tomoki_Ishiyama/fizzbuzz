/**
 *
 */
package jp.co.fizzbuzz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author ishiyama.tomoki
 *
 */
class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * {@link jp.co.fizzbuzz.FizzBuzz#main(java.lang.String[])} のためのテスト・メソッド。
	 */
	@Test
	public void isFizz() {
        System.out.println("isFizz(9) :");
        System.out.println(FizzBuzz.checkFizzBuzz(9));
        assertEquals(FizzBuzz.checkFizzBuzz(9),"Fizz" );
    }
	/**
	 * {@link jp.co.fizzbuzz.FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。
	 */

	@Test
	public void isBuzz() {
        System.out.println("isBuzz(20) :");
        System.out.println(FizzBuzz.checkFizzBuzz(20));
        assertEquals(FizzBuzz.checkFizzBuzz(20),"Buzz" );
    }
	@Test
	public void isFizzBuzz() {
        System.out.println("isFizzBuzz(45) :");
        System.out.println(FizzBuzz.checkFizzBuzz(45));
        assertEquals(FizzBuzz.checkFizzBuzz(45),"FizzBuzz" );
    }
	@Test
	public void isNum_44() {
        System.out.println("isNum_44 :");
        System.out.println(FizzBuzz.checkFizzBuzz(44));
        assertEquals(FizzBuzz.checkFizzBuzz(44),"44");
    }
	@Test
	public void isNum_46() {
        System.out.println("isNum_46 :");
        System.out.println(FizzBuzz.checkFizzBuzz(46));
        assertEquals(FizzBuzz.checkFizzBuzz(46),"46" );
    }

	/*
	@Test
	public void isHundredNum() {
		for(int i=0; i<50; i++) {
			System.out.println(i +":"+FizzBuzz.checkFizzBuzz(i));
		}
	}
*/
}
